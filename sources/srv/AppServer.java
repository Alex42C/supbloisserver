package srv;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class AppServer {

    private ServerSocket server = null;
    private boolean isRunning = true;


    public AppServer(String pHost, int pPort){
        try {
            server = new ServerSocket(pPort, 100, InetAddress.getByName(pHost));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //On lance notre serveur
    public void open(){

        Thread t = new Thread(() -> {
            while(isRunning){

                try {
                    //On attend une connexion d'un client
                    Socket client = server.accept();
                    System.out.printf("Received connection request from %s .%n", client.getInetAddress().toString());

                    Thread t1 = new Thread(new ClientHandler(client));
                    t1.start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
                server = null;
            }
        });

        t.start();
    }

    public void close(){
        isRunning = false;
    }
}