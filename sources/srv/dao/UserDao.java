package srv.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements Dao<User> {
    @Override
    public User get(Object key) {
        long k = (long) key;
        try {
            PreparedStatement statement = ConnectionMYSQL.getConnection().prepareStatement("Select * FROM users WHERE ID = ?;");
            statement.setLong(1, k);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            return new User(resultSet.getLong("ID"), resultSet.getString("PHONE"), resultSet.getString("FIRSTNAME"), resultSet.getString("LASTNAME"), resultSet.getString("PASSWORDHASH"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public User find(String phone){
        try {
            PreparedStatement statement = ConnectionMYSQL.getConnection().prepareStatement("Select * FROM users WHERE Phone = ?;");
            statement.setString(1, phone);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            return new User(resultSet.getLong("ID"), resultSet.getString("PHONE"), resultSet.getString("FIRSTNAME"), resultSet.getString("LASTNAME"), resultSet.getString("PASSWORDHASH"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<User> getAll() {
        return getAll("users", User.class);
    }

    @Override
    public boolean create(User obj) {
        try {
            Connection connection = ConnectionMYSQL.getConnection();

            Statement stat = connection.createStatement();
            stat.executeUpdate("INSERT INTO app.users (PHONE, FIRSTNAME, LASTNAME, PASSWORDHASH) VALUE ("+
                    String.format("'%s', '%s', '%s', '%s');",
                            obj.getPhone(),
                            obj.getFirstname(),
                            obj.getLastname(),
                            obj.getPasswordhash()));
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean update(Object key, User newUsr) {
        try {
            long k = (long) key;
            Statement statement = ConnectionMYSQL.getConnection().createStatement();
            statement.executeUpdate(String.format("UPDATE app.USERS SET PHONE = '%s', FIRSTNAME = '%s', LASTNAME = '%s', PASSWORDHASH = '%s' WHERE ID = %s;",
                    newUsr.getPhone(),
                    newUsr.getFirstname(),
                    newUsr.getLastname(),
                    newUsr.getPasswordhash(),
                    String.valueOf(k)));
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void delete(Object key) {
        try {
            long k = (long) key;
            Statement statement = ConnectionMYSQL.getConnection().createStatement();
            statement.executeUpdate("DELETE FROM users WHERE ID = " + String.valueOf(k) + ";");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
