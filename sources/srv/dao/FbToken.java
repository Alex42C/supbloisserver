package srv.dao;

/**
 * Fait par Alexandre le 04/01/2018.
 */
public class FbToken {
    long id;
    String token;

    @DaoConstructor
    public FbToken(long id, String token) {
        this.id = id;
        this.token = token;
    }

    public long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }
}
