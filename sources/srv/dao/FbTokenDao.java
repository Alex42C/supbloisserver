package srv.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 * Fait par Alexandre le 04/01/2018.
 */
public class FbTokenDao implements Dao<FbToken> {
    @Override
    public List<FbToken> getAll() {
        return null;
    }

    @Override
    public boolean create(FbToken obj) {
        return false;
    }

    @Override
    public boolean update(Object key, FbToken newObj) {
        return false;
    }

    public boolean update(Long id, String token){
        try {
            PreparedStatement statement = ConnectionMYSQL.getConnection().prepareStatement("INSERT INTO fb_tokens (USER_ID, TOKEN) values (?, ?) ON DUPLICATE KEY UPDATE TOKEN = ?;");
            statement.setLong(1, id);
            statement.setString(2, token);
            statement.setString(3, token);

            statement.executeUpdate();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void delete(Object key) {

    }

    @Override
    public FbToken get(Object key) {
        try {
            long k = (long) key;
            PreparedStatement statement = ConnectionMYSQL.getConnection().prepareStatement("Select * FROM fb_tokens WHERE USER_ID = ?;");
            statement.setLong(1, k);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            return new FbToken(resultSet.getLong("USER_ID"), resultSet.getString("TOKEN"));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
