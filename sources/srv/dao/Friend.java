package srv.dao;

public class Friend
{
    private long user_id, friend_id;

    @DaoConstructor
    public Friend(long user_id, long friend_id) {
        this.user_id = user_id;
        this.friend_id = friend_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public long getFriend_id() {
        return friend_id;
    }
}
