package srv.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Fait par Alexandre le 02/01/2018.
 */
public class SyncDao implements Dao<Sync> {
    @Override
    public List<Sync> getAll() {
        return getAll("sync", Sync.class);
    }

    @Override
    public boolean create(Sync obj) {
        return false;
    }

    @Override
    public boolean update(Object key, Sync newObj) {

        try {
            long k = (long) key;
            Statement statement = ConnectionMYSQL.getConnection().createStatement();
            statement.executeUpdate(String.format("INSERT INTO app.sync (ID, LAST_SYNC) values (%s, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE LAST_SYNC = CURRENT_TIMESTAMP;", String.valueOf(k)));
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void delete(Object key) {

    }

    @Override
    public Sync get(Object key) {
        long k = (long) key;
        try {
            PreparedStatement statement = ConnectionMYSQL.getConnection().prepareStatement("Select * FROM sync WHERE ID = ?;");
            statement.setLong(1, k);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            return new Sync(resultSet.getLong("ID"), resultSet.getTimestamp("last_Sync"));
        } catch (SQLException e) {
            //e.printStackTrace();
            return null;
        }
    }
}
