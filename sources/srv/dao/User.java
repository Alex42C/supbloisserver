package srv.dao;

public class User {
    private long id;
    private String phone, firstname, lastname, passwordhash;

    @DaoConstructor
    public User(long id, String phone, String firstname, String lastname, String passwordhash) {
        this.id = id;
        this.phone = phone;
        this.firstname = firstname;
        this.lastname = lastname;
        this.passwordhash = passwordhash;
    }

    public User(String phone, String firstname, String lastname, String passwordhash) {
        this.phone = phone;
        this.firstname = firstname;
        this.lastname = lastname;
        this.passwordhash = passwordhash;
    }

    public long getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPasswordhash() {
        return passwordhash;
    }
}
