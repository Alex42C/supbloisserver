package srv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageDao implements Dao<Message> {
    @Override
    public Message get(Object key) {
        return null;
    }

    public List<Message> forUser(long id){
        try {
            PreparedStatement statement = ConnectionMYSQL.getConnection().prepareStatement("Select * FROM messages WHERE DEST_ID = ?;");
            statement.setLong(1, id);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            List<Message> messages = new ArrayList<>();

            while (resultSet.next()){
                messages.add(new Message(resultSet.getLong("MSG_ID"), resultSet.getLong("SENDER_ID"), resultSet.getLong("DEST_ID"), resultSet.getString("MSG"), resultSet.getTimestamp("TIME")));
            }

            return messages;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Message> getAll() {
        return getAll("messages", Message.class);
    }

    @Override
    public boolean create(Message obj) {
        String querry = "INSERT INTO messages (SENDER_ID, DEST_ID, MSG) VALUE (?,?,?);";
        try {
            Connection connection = ConnectionMYSQL.getConnection();
            PreparedStatement stat = connection.prepareStatement(querry);
            stat.setLong(1, obj.getSender_id());
            stat.setLong(2, obj.getDest_id());
            stat.setString(3, obj.getMsg());
            stat.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean update(Object key, Message newObj) {
        return false;
    }

    @Override
    public void delete(Object key) {

    }
}
