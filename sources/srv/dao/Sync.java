package srv.dao;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Fait par Alexandre le 02/01/2018.
 */
public class Sync {
    long id;
    LocalDateTime last_sync;

    public long getId() {
        return id;
    }

    public LocalDateTime getLast_sync() {
        return last_sync;
    }

    public Sync() {
    }

    @DaoConstructor
    Sync(long id, String last_sync){
        this.id = id;
        this.last_sync = Timestamp.valueOf(last_sync).toLocalDateTime();
    }

    Sync(long id, Timestamp timestamp){
        this.id = id;
        last_sync = timestamp.toLocalDateTime();
    }
}
