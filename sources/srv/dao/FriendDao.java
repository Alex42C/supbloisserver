package srv.dao;

import srv.Pair;

import java.sql.*;
import java.util.List;

public class FriendDao implements Dao<Friend> {
    @Override
    public List<Friend> getAll() {
        return getAll("friends", Friend.class);
    }

    @Override
    public boolean create(Friend obj) {
        String querry = "INSERT INTO friends (USER_ID, FRIEND_ID) VALUE (?,?);";
        try {
            Connection connection = ConnectionMYSQL.getConnection();
            PreparedStatement stat = connection.prepareStatement(querry);
            stat.setLong(1, obj.getUser_id());
            stat.setLong(2, obj.getFriend_id());
            stat.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean update(Object key, Friend newObj) {
        return false;
    }

    @Override
    public void delete(Object key) {
        try {
            Pair<Long, Long> k = (Pair<Long, Long>) key;
            Statement statement = ConnectionMYSQL.getConnection().createStatement();
            statement.executeUpdate("DELETE FROM friends WHERE USER_ID = " + String.valueOf(k.getFirst()) + " AND FRIEND_ID = "+String.valueOf(k.getSecond())+";");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Friend get(Object key) {
        return null;
    }
}
