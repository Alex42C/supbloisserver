package srv;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Run {
    public static void main(String[] args) throws IOException {
       InputStream input;
        input = Run.class.getClassLoader().getResourceAsStream("server.properties");

        Properties properties = new Properties();
        try {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String host = properties.getProperty("host");
        int port = Integer.parseInt(properties.getProperty("port"));

        AppServer server = new AppServer(host, port);
        server.open();
    }
}
