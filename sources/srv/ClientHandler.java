package srv;

import com.google.gson.Gson;
import srv.cmd.*;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;

/**
 * Fait par Alexandre le 01/12/2017.
 */
public class ClientHandler implements Runnable{
    private static long lastID = 0;
    public static Gson gson = new Gson();

    private HashMap<String, CommandHandler> commandHandlers;
    private Socket socket;
    private PrintWriter writer = null;
    private BufferedReader reader = null;
    private long id;


    ClientHandler(Socket pSock){
        socket = pSock;
        id = lastID++;
        commandHandlers = new HashMap<>();

        //Register Command Handlers
        registerCommandHandler(new LoginCH(), "LOGIN");
        registerCommandHandler(new PushCH(), "PUSH");
        registerCommandHandler(new PullAllCH(), "PULL");
        registerCommandHandler(new CreateAccCH(), "CREATE");
        registerCommandHandler(new AddFriendCH(), "FRIEND_ADD");
        registerCommandHandler(new FirebaseCH(), "FB_TOKEN");

        System.out.printf("ID=%d MSG='Connexion au client ouverte.'%n", id);
    }

    public void run(){
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(!socket.isClosed()){
            try {
                writer = new PrintWriter(socket.getOutputStream());

                String querry = reader.readLine();
                if (querry == null)
                    continue;
                System.err.println("ID="+id+" Received Command = '"+querry+"'");

                if(querry.equals("CLOSE")){
                    writer.write("CLOSED");
                    writer.flush();
                    writer = null;
                    reader = null;
                    socket.close();
                    System.out.printf("ID=%d MSG='Connexion au client fermee.'%n", id);
                    break;
                }


                String toSend;

                Command cmd = gson.fromJson(querry, Command.class);


                try {
                    if(commandHandlers.get(cmd.getType()) != null){
                        toSend = commandHandlers.get(cmd.getType()).excecute(cmd.getParameters());
                    }
                    else{
                        toSend = "UNKNOWN";
                    }
                }catch (Exception e){
                    toSend = "FAILED";
                    System.err.println("ID="+id+" Command failed");
                    e.printStackTrace();
                }

                writer.write(toSend + '\n');
                writer.flush();

            }catch(SocketException e){
                System.err.println("ID="+id+" Connexion Unexpectedly ended !");
                break;
            } catch (IOException e) {
                System.err.printf("!!! Exception !!! ID=%d %n", id);
                e.printStackTrace();
            }
        }
        System.err.printf("ID=%d Client Handler terminated !%n", id);
    }

    public boolean verify(Object t){
        try {
            long tok = Long.parseLong((String) t);
            LoginCH l = (LoginCH) getCommandHandler("LOGIN");
            return tok == l.getToken();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public long getId(){
        try {
            return ((LoginCH) getCommandHandler("LOGIN")).getId();
        }catch (Exception e){
            return -1;
        }
    }

    public void registerCommandHandler(CommandHandler ch, String type){
        commandHandlers.put(type, ch);
        ch.setClientHandler(this);
    }

    public CommandHandler getCommandHandler(String key) {
        return commandHandlers.get(key);
    }
}
