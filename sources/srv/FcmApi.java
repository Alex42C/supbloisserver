package srv;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FcmApi {
    public static final String key;
    public static final String url;

    static {
        InputStream input;
        input = FcmApi.class.getClassLoader().getResourceAsStream("fcmjava.properties");

                Properties properties = new Properties();
        try {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        key = properties.getProperty("fcm.api.key");
        url = properties.getProperty("fcm.api.url");

    }
}
