package srv.cmd;

import com.google.gson.Gson;
import srv.dao.User;
import srv.dao.UserDao;

import java.security.SecureRandom;

public class LoginCH extends CommandHandler{
    boolean logged = false;
    long id = -1;
    long token;

    public LoginCH() {
    }

    /**
     * Arg0 phone
     * Arg1 password hash
     * @param args
     * @return
     */
    @Override
    public String excecute(Object... args) {
        if (logged)
            return new Gson().toJson("LOGIN_OK_" + String.valueOf(token));
        User user = null;
        String phone = (String) args[0], hash = (String) args[1];
        //System.out.printf("Login demand : %s | %s %n", phone, hash);
        for (User u : new UserDao().getAll()) {
            if (u.getPhone().equals(phone))
                user = u;
        }
        if (user != null){
            id = user.getId();
            if (user.getPasswordhash().toLowerCase().equals(hash.toLowerCase())){
                logged = true;
                token = new SecureRandom().nextLong();
                return new Gson().toJson("LOGIN_OK_" + String.valueOf(token));
            }
        }
        return "LOGIN_FAILED";
    }

    public long getToken() {
        return token;
    }

    public long getId() {
        return id;
    }
}
