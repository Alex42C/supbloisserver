package srv.cmd;

import com.google.gson.Gson;
import srv.FriendAndroid;
import srv.dao.Friend;
import srv.dao.FriendDao;
import srv.dao.User;
import srv.dao.UserDao;

public class AddFriendCH extends CommandHandler{

    @Override
    public String excecute(Object... args) {
        if (!ch.verify(args[0])){
            return "BAD_TOKEN";
        }
        User ami = new UserDao().find((String) args[1]);

        if (ami == null)
            return new Gson().toJson(null);

        new FriendDao().create(new Friend(ch.getId(), ami.getId()));

        return new Gson().toJson(new FriendAndroid(ami.getPhone(), ami.getFirstname(), ami.getLastname()));
    }
}
