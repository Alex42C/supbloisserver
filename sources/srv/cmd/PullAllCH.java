package srv.cmd;


import com.google.gson.Gson;
import srv.MessageAndroid;
import srv.dao.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class PullAllCH extends CommandHandler{

    @Override
    public String excecute(Object... args) {
        if (ch.verify(args[0])){
            long id = ((LoginCH) ch.getCommandHandler("LOGIN")).getId();
            Sync sync = new SyncDao().get(id);
            List<Message> messages = new MessageDao().forUser(id);
            if (sync != null && !(boolean)args[1]) {
                LocalDateTime last = sync.getLast_sync();
                messages.removeIf(message -> message.getTime().isBefore(last));
            }
            new SyncDao().update(id, new Sync());
            List<MessageAndroid> res = toAndroidFormat(messages);
            if (args.length > 2) {
                String s = (String) args[2];
                res.removeIf(messageAndroid -> !s.equals(messageAndroid.getSenderId()));
            }
            return new Gson().toJson(res.toArray());

        }else
            return "BAD_TOKEN";
    }

    public static List<MessageAndroid> toAndroidFormat(List<Message> input){
        List<MessageAndroid> result = new ArrayList<>();
        for (Message message : input) {
            try {
                String senderPhone = new UserDao().get(message.getSender_id()).getPhone();
                result.add(new MessageAndroid(message.getMsg_id(), senderPhone, Date.from(message.getTime().atZone(ZoneId.systemDefault()).toInstant()), message.getMsg()));
            } catch (NullPointerException ignored){
                System.err.printf("Couldn't find user with id %d%n", message.getSender_id());
            }

        }
        return result;
    }
}
