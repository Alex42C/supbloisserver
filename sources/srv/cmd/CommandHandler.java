package srv.cmd;

import srv.ClientHandler;

public abstract class CommandHandler {
    protected String type;
    int nArgs;
    protected ClientHandler ch;

    public abstract String excecute(Object... args);

    public String getType() {
        return type;
    }

    public int getnArgs() {
        return nArgs;
    }

    public ClientHandler getClientHandler() {
        return ch;
    }

    public void setClientHandler(ClientHandler ch) {
        this.ch = ch;
    }
}