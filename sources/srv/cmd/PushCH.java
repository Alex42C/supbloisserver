package srv.cmd;

import com.google.gson.Gson;
import org.riversun.fcm.FcmClient;
import org.riversun.fcm.model.EntityMessage;
import org.riversun.fcm.model.FcmResponse;
import srv.FcmApi;
import srv.dao.*;

public class PushCH extends CommandHandler {
    @Override
    public String excecute(Object... args) {
        if (!ch.verify(args[0]))
            return "BAD_TOKEN";

        String msg = (String) args[1];

        UserDao userDao = new UserDao();
        long destId = userDao.find((String) args[2]).getId();
        long me = ((LoginCH)ch.getCommandHandler("LOGIN")).id;
        Message message = new Message(me, destId, msg);

        boolean success = false;
        if (msg.length() > 0)
            success = new MessageDao().create(message);

        if (success) {
            new Thread(() -> {
                FbToken to = new FbTokenDao().get(destId);
                if (to != null){
                    FcmClient client = new FcmClient(FcmApi.url);
                    client.setAPIKey(FcmApi.key);
                    EntityMessage entityMessage = new EntityMessage();
                    entityMessage.addRegistrationToken(to.getToken());
                    entityMessage.putStringData("sender", userDao.get(me).getPhone());
                    entityMessage.putStringData("content", msg);
                    FcmResponse response = client.pushToEntities(entityMessage);
                    System.out.println("FireBase Response = " + response.getJson());
                }
            }).start();
        }

        return new Gson().toJson(success);
    }
}
