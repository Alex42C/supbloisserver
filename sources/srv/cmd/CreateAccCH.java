package srv.cmd;

import com.google.gson.Gson;
import srv.dao.User;
import srv.dao.UserDao;

/**
 * Fait par Alexandre le 04/01/2018.
 */
public class CreateAccCH extends CommandHandler{

    @Override
    public String excecute(Object... args) {
        UserDao userDao = new UserDao();
        Gson gson = new Gson();

        String phone = (String) args[1];
        String prenom = (String) args[2];
        String nom = (String) args[3];
        String pwd = (String) args[4];

        if (userDao.find(phone) != null)
            return gson.toJson(false);

        boolean success = userDao.create(new User(phone, prenom, nom, pwd));

        return gson.toJson(success);
    }
}
