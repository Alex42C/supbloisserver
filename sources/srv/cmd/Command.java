package srv.cmd;

public class Command{
    String type;
    Object[] parameters;

    public Command(String type, Object[] parameters) {
        this.type = type;
        this.parameters = parameters;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public String getType() {

        return type;
    }
}
