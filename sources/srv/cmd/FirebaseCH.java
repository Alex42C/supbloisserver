package srv.cmd;

import com.google.gson.Gson;
import srv.dao.FbTokenDao;

public class FirebaseCH extends CommandHandler {
    @Override
    public String excecute(Object... args) {
        if (!ch.verify(args[0]))
            return "BAD_TOKEN";
        String newToken = (String) args[1];

        return new Gson().toJson(new FbTokenDao().update(ch.getId(), newToken));
    }
}
